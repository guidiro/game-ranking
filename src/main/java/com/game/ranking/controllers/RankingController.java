package com.game.ranking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.game.ranking.models.Ranking;
import com.game.ranking.repositories.RankingRepository;

@RestController
public class RankingController {

	@Autowired
	RankingRepository rankingRepository;
	

	@RequestMapping(method = RequestMethod.POST, path = "/ranking")
	public Ranking criarRanking(@Validated @RequestBody Ranking ranking) {
		
		return rankingRepository.save(ranking);
			
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/rankingAll")
	public ResponseEntity<?> buscarRanking() {
		Iterable<Ranking> interableRanking = rankingRepository.findTop10ByOrderByPontuacaoDesc();

		return ResponseEntity.ok().body(interableRanking);
	}
	
	@RequestMapping(method=RequestMethod.GET, path = "/rankingJogo/{nomeJogo}")
	public ResponseEntity<?> buscarRankingJogo(@PathVariable String nomeJogo){ 
	Iterable<Ranking> interableRanking = rankingRepository.findAllBynomeJogo(nomeJogo);
	
		return ResponseEntity.ok().body(interableRanking);
	}
	
	@RequestMapping(method=RequestMethod.GET, path = "/rankingJogador/{nomeJogador}")
	public ResponseEntity<?> buscarRankingJogador(@PathVariable String nomeJogador){ 
	Iterable<Ranking> interableRanking = rankingRepository.findAllBynomeJogador(nomeJogador);
	
		return ResponseEntity.ok().body(interableRanking);
	}


}
