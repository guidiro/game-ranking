package com.game.ranking.repositories;

import org.springframework.data.repository.CrudRepository;

import com.game.ranking.models.Ranking;

public interface RankingRepository extends CrudRepository<Ranking, Long>{

	Iterable<Ranking> findTop10ByOrderByPontuacaoDesc();

	Iterable<Ranking> findAllBynomeJogo(String nomeJogo);

	Iterable<Ranking> findAllBynomeJogador(String nomeJogador);

	
}
